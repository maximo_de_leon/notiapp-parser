package model;

import util.UrlShortener;

/**
 * Created by maximodeleon on 8/23/16.
 */
public class Article {

    String title;
    String description;
    String shortDescription;
    String imageUrl;
    //String newspaperLogoUrl;
    String longArticleUrl;
    String shortArticleUrl;
    long pubDate;
    String periodico;
    int type;

    public Article(String title, String description, String imageUrl
            /*, String newspaperLogoUrl*/, String articleUrl, long pubDate, String ref, int type, String entryContent) {
        this.title = title;
        this.description = description;
        this.imageUrl = imageUrl;
        //this.newspaperLogoUrl = newspaperLogoUrl;
        this.longArticleUrl = articleUrl;
        this.shortArticleUrl = UrlShortener.shortenUrl(articleUrl);
        this.pubDate = pubDate;
        this.periodico = ref;
        this.type = type;
        this.shortDescription = entryContent;
    }

    public long getPubDate() {
        return pubDate;
    }

    public void setPubDate(long pubDate) {
        this.pubDate = pubDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    /*
    public String getNewspaperLogoUrl() {
        return newspaperLogoUrl;
    }

    public void setNewspaperLogoUrl(String newspaperLogo) {
        this.newspaperLogoUrl = newspaperLogo;
    }
*/
    public String getLongArticleUrl() {
        return longArticleUrl;
    }

    public void setLongArticleUrl(String longArticleUrl) {
        this.longArticleUrl = longArticleUrl;
    }

    public String getShortArticleUrl() {
        return shortArticleUrl;
    }

    public void setShortArticleUrl(String shortArticleUrl) {
        this.shortArticleUrl = shortArticleUrl;
    }

    public String getPeriodico() {
        return periodico;
    }

    public void setPeriodico(String newspaperRef) {
        this.periodico = newspaperRef;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getShortDescription() { return  shortDescription;}

}
