package model;

/**
 * Created by maximodeleon on 10/1/16.
 */
public class Types {

    public  enum ArticleTypes {
        IMAGE(1), NOIMAGE(2), AD(3);
        private int value ;

        private ArticleTypes(int val) {
            this.value = val;
        }

        public int getValue() {
            return this.value;
        }
    };
}
