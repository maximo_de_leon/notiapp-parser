package main;

import com.google.gson.Gson;
import model.Article;
import parsers.FeedParser;
import textsummary.SummaryTool;
import util.Logger;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by maximodeleon on 8/22/16.
 */
public class Main {

    public static void main(String [] args) {

        System.setProperty("http.agent", "Mozilla");

        Logger logger = Logger.getInstance(Main.class);
        //ParseManager manager = new ParseManager();
        //manager.parseAll();.

        List<Article> articleList = new ArrayList<>();

        FeedParser parser = new FeedParser();

        /*Metro tiene un feed desactualizado, investigar mejor y luego poner*/
        //articleList.addAll(parser.parse("https://www.metrord.do/do/rss/noticias.xml", "Metro"));
        //articleList.addAll(parser.parse("http://feeds.feedburner.com/noticiassin1", "SIN"));
        //articleList.addAll(parser.parse("http://www.listindiario.com/rss/portada/", "Listin Diario"));
        //articleList.addAll(parser.parse("http://hoy.com.do/feed/", "Hoy"));
        //articleList.addAll(parser.parse("https://elnuevodiario.com.do/feed/", "El Nuevo Diario"));
        //articleList.addAll(parser.parse("http://elnacional.com.do/feed/", "El Nacional"));
        //articleList.addAll(parser.parse("http://eldia.com.do/feed/", "El Dia"));
        //articleList.addAll(parser.parse("http://www.elcaribe.com.do/rss", "El Caribe"));
        //articleList.addAll(parser.parse("https://www.diariolibre.com/rss/portada.xml", "Diario Libre"));
        articleList.addAll(parser.parse("http://acento.com.do/feed/", "Acento"));

        new Gson().toJson(articleList);
        logger.logInfo("Proceso finalizado exitosamente");
        logger.printStats();
    }

}
