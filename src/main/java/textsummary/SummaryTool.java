package textsummary;

import java.text.BreakIterator;
import java.util.*;

public class SummaryTool{

    private ArrayList<Sentence> sentences, contentSummary;
    private ArrayList<Paragraph> paragraphs;
    private int noOfSentences, noOfParagraphs;
    private double[][] intersectionMatrix;
    private LinkedHashMap<Sentence,Double> dictionary;
    private  final String PARAGRAPH_SPLIT_REGEX = "\n{1,}";


    public SummaryTool(){
        noOfSentences = 0;
        noOfParagraphs = 0;
        sentences = new ArrayList<Sentence>();
        paragraphs = new ArrayList<Paragraph>();
        contentSummary = new ArrayList<Sentence>();
        dictionary = new LinkedHashMap<Sentence,Double>();
    }

    /*Gets the sentences from the entire passage*/
    private void extractSentenceFromContext(String context){

        String [] pars = context.split(PARAGRAPH_SPLIT_REGEX);
        BreakIterator iterator = BreakIterator.getSentenceInstance();

        for (int i =0; i < pars.length; i ++) {
            Paragraph paragraph = new Paragraph(i);


            iterator.setText(pars[i]);
            int start = iterator.first();

            for (int end = iterator.next(); end != BreakIterator.DONE; start = end, end=iterator.next()) {
                sentences.add(new Sentence(end, pars[i].substring(start, end), i));
                noOfSentences++;
                paragraph.sentences.add(new Sentence(end, context.substring(start, end), i));
            }
            paragraphs.add(paragraph);

        }
        noOfParagraphs = pars.length - 1;

    }

    private double noOfCommonWords(Sentence str1, Sentence str2){
        double commonCount = 0;

        for(String str1Word : str1.value.split("\\s+")){
            for(String str2Word : str2.value.split("\\s+")){
                if(str1Word.compareToIgnoreCase(str2Word) == 0){
                    commonCount++;
                }
            }
        }

        return commonCount;
    }

    private void createIntersectionMatrix(){
        intersectionMatrix = new double[noOfSentences][noOfSentences];
        for(int i=0;i<noOfSentences;i++){
            for(int j=0;j<noOfSentences;j++){

                if(i<=j){
                    Sentence str1 = sentences.get(i);
                    Sentence str2 = sentences.get(j);
                    intersectionMatrix[i][j] = noOfCommonWords(str1,str2) / ((double)(str1.noOfWords + str2.noOfWords) /2);
                }else{
                    intersectionMatrix[i][j] = intersectionMatrix[j][i];
                }

            }
        }
    }

    private void createDictionary(){
        for(int i=0;i<noOfSentences;i++){
            double score = 0;
            for(int j=0;j<noOfSentences;j++){
                score+=intersectionMatrix[i][j];
            }
            dictionary.put(sentences.get(i), score);
            ((Sentence)sentences.get(i)).score = score;
        }
    }

    public void createSummary(String context){


        this.extractSentenceFromContext(context);
        //this.printSentences();
        this.createIntersectionMatrix();
        this.createDictionary();


        for(int j=0;j<=noOfParagraphs;j++){
            int primary_set = paragraphs.get(j).sentences.size()/5;

            //Sort based on score (importance)
            Collections.sort(paragraphs.get(j).sentences,new SentenceComparator());
            for(int i=0;i<=primary_set;i++){
                if (paragraphs.get(j).sentences.size() > 0){
                    contentSummary.add(paragraphs.get(j).sentences.get(i));
                }
            }
        }

        //To ensure proper ordering
        Collections.sort(contentSummary,new SentenceComparatorForSummary());

    }

    public String getSummary() {
        return contentSummary.get(contentSummary.size() -1).value;
    }

    public void printSummary(){
        System.out.println("no of paragraphs = "+ noOfParagraphs);
        for(Sentence sentence : contentSummary){
            System.out.println(sentence.value);
        }
    }


}
