package textsummary;

public class Sentence{
    int paragraphNumber;
    int number;
    double score;
    int noOfWords;
    String value;

    Sentence(int number, String value, int paragraphNumber){
        this.number = number;
        this.value = new String(value);
        noOfWords = value.split("\\s+").length;
        score = 0.0;
        this.paragraphNumber = paragraphNumber;
    }
}