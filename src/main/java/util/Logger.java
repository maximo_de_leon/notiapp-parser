package util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by maximodeleon on 11/21/16.
 */
public class Logger {

    public static Logger instance = null;
    private Class clazz;
    private final SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.YYYY KK:mm:ss a");
    private  int errors = 0;
    private int warnings = 0;

    enum LogLevel {
        ERROR("Error"),
        WARNING("Warning"),
        INFO("Info");

        private String levelStr;

        LogLevel(String str) {
            levelStr = str;
        }
    };

    private Logger(Class clazz) {
        this.clazz = clazz;
    }

    public static synchronized Logger getInstance(Class clazz){
        if (instance == null) {
            instance = new Logger(clazz);
        } else {
            instance.setClazz(clazz);
        }
        return instance;
    }

    private void setClazz(Class clazz) {
        this.clazz = clazz;
    }

    public void logError(String msg) {
        logMessage(msg, LogLevel.ERROR);
        errors++;
    }

    public void logError(String msg, Exception e) {
        logMessage(msg, LogLevel.ERROR);
        e.printStackTrace();
        errors++;
    }

    public void logInfo(String msg) {
        logMessage(msg, LogLevel.INFO);
    }

    public void logWarning(String msg) {
        logMessage(msg, LogLevel.WARNING);
        warnings++;
    }

    private void logMessage(String msg, LogLevel level) {
        Date date = Calendar.getInstance().getTime();
        System.out.println(formatter.format(date) + " - " + level + " " +clazz.getSimpleName() + " " +  ": " + msg);
    }

    public void printStats () {
        logMessage("Se produjeron " + errors + " errores y  " + warnings + " warnings", LogLevel.INFO);
    }

    public void logElapsedTime(Date startDate, Date endDate) {

        long different = endDate.getTime() - startDate.getTime();

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;

        System.out.printf(
                "%d dias, %d horas, %d minutos, %d segundos%n",
                elapsedDays,
                elapsedHours, elapsedMinutes, elapsedSeconds);
    }

}
