package util;

/**
 * Created by maximodeleon on 12/1/16.
 */
public class Constants {

    public static final String SERVICE_JSON_PATH_PROPERTY_KEY = "servicefilepath";
    public static final String DATABASE_PROPERTY_KEY = "databaseurl";
    public static final String MAIN_REF = "/articulos";

    public static final String EL_CARIBE_KEY = "El Caribe";
    public static final String LISTIN_DIARIO_KEY = "Listin Diario";
    public static final String EL_DIA_KEY = "El Dia";
    public static final String DIARIO_LIBRE_KEY = "Diario Libre";
    public static final String HOY_KEY = "Hoy";
    public static final String METRO_KEY = "Metro";
    public static final String ACENTO_KEY = "Acento";
    public static final String EL_NACIONAL_KEY = "El Nacional";
    public static final String SIN_KEY = "SIN";
    public static final String EL_NUEVO_DIARIO_KEY = "El Nuevo Diario";

}
