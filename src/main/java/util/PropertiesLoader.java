package util;

import main.Main;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by maximodeleon on 12/1/16.
 */
public class PropertiesLoader {

    private static final String CONFIG_PROPERTY_FILENAME = "/config.properties";
    private static Properties props = null;

    public static String getProperty(String propName) {
        if (props == null) {
            props = getProperties();
        }
        return props.getProperty(propName);
    }

    private static Properties getProperties() {
        Logger logger  = Logger.getInstance(PropertiesLoader.class);
        props = new Properties();
        InputStream is;
        try {
            is = Main.class.getResourceAsStream(CONFIG_PROPERTY_FILENAME);
            props.load(is);
        }catch (FileNotFoundException e) {
            logger.logError("Se produjo un error leyendo el archivo .properties", e);
            System.exit(-1);

        }catch (IOException e) {
            logger.logError("Se produjo un error leyendo el archivo .properties", e);
            System.exit(-1);
        }
        return props;
    }

}
