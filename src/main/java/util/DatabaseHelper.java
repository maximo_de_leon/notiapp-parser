package util;

import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import main.Main;

import java.io.InputStream;
import java.util.List;

/**
 * Created by maximodeleon on 12/1/16.
 */
public class DatabaseHelper {

    public static DatabaseHelper instance = null;

    private DatabaseReference mainRef;
    private Logger logger = Logger.getInstance(DatabaseHelper.class);

    public static DatabaseHelper getInstance() {
        if (instance == null) {
            instance = new DatabaseHelper();
        }

        return instance;
    }

    private DatabaseHelper() {

        String serviceJson = PropertiesLoader.getProperty(Constants.SERVICE_JSON_PATH_PROPERTY_KEY);
        String database = PropertiesLoader.getProperty(Constants.DATABASE_PROPERTY_KEY);

        InputStream is = Main.class.getResourceAsStream(serviceJson);
        FirebaseOptions options = new FirebaseOptions.Builder()
                .setServiceAccount(is)
                .setDatabaseUrl(database)
                .build();
        FirebaseApp.initializeApp(options);

        mainRef = FirebaseDatabase.getInstance().getReference(Constants.MAIN_REF);
    }

    public void insertItemsInPath (String path, List<? extends Object> objects) {
        logger.logInfo("Se van a empezar a insertar los articulos (" + path + ")");
        logger.logInfo(objects.size() + " articulos");
        mainRef.child(path).setValue(null);
        for (Object object : objects) {
            insertItemInPath(path, object);
        }
        logger.logInfo("Se insertaron todos los articulos");
    }

    private void insertItemInPath(String path, Object object) {
        mainRef.child(path).push().setValue(object);
    }

}
