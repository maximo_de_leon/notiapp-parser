package util;

import com.google.gson.Gson;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;

/**
 * Created by maximodeleon on 10/23/16.
 */
public class UrlShortener {

    private static final String GOOGLE_URL_SHORT_API = "https://www.googleapis.com/urlshortener/v1/url";
    private static final String GOOGLE_API_KEY = "AIzaSyBy39476ix415QDmaBlPQVMDeNJGoTmw8Y";


    public static String shortenUrl(String url) {
        String shortUrl  = "";
        if (url == null) {
            shortUrl = url;
        }else if(!url.startsWith("http://") && !url.startsWith("https://")){
            url = "http://"+url;
        }
        try {
            String json = "{\"longUrl\": \"" + url + "\"}";
            String apiURL = GOOGLE_URL_SHORT_API + "?key=" + GOOGLE_API_KEY;
            HttpPost postRequest = new HttpPost(apiURL);
            postRequest.setHeader("Content-Type", "application/json");
            postRequest.setEntity(new StringEntity(json, "UTF-8"));

            CloseableHttpClient httpClient = HttpClients.createDefault();
            HttpResponse response = httpClient.execute(postRequest);
            String responseText = EntityUtils.toString(response.getEntity());

            Gson gson = new Gson();
            @SuppressWarnings("unchecked")
            HashMap<String, String> res = gson.fromJson(responseText, HashMap.class);

            shortUrl = res.get("id");


        }
        catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        return  shortUrl;
    }
}
