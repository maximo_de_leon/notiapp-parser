package parsers;

import com.google.gson.Gson;
import model.Article;
import model.Types;
import util.Constants;
import util.DatabaseHelper;
import util.Logger;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

/**
 * Created by maximodeleon on 11/30/16.
 */
public class ParseManager {

    private final String jsonFileName = "articles.json";
   private final Map<String, Parser> newspaperMap = new HashMap<String, Parser>() {{
                    put(Constants.EL_CARIBE_KEY, new ElCaribeParser());
                    put(Constants.LISTIN_DIARIO_KEY, new ListinDiarioParser());
                    put(Constants.EL_DIA_KEY, new EldiaParser());
                    put(Constants.DIARIO_LIBRE_KEY, new DiarioLibreParser());
                    put(Constants.HOY_KEY, new HoyParser());
                    put(Constants.METRO_KEY, new MetroParser());
                    put(Constants.ACENTO_KEY, new AcentoParser());
                    put(Constants.EL_NACIONAL_KEY, new ElNacionalParser());
                    put(Constants.SIN_KEY, new NoticiasSINParser());
                    put(Constants.EL_NUEVO_DIARIO_KEY, new ElNuevoDiarioParser());
                }};

    Comparator<Article> comparator = new Comparator<Article>() {
        @Override
        public int compare(Article o1, Article o2) {
            Long pubDate1 = Long.valueOf(o1.getPubDate());
            Long pubDate2 = Long.valueOf(o2.getPubDate());
          return pubDate2.compareTo(pubDate1);
        }
    };

    public void parseAll() {
        Set<String> keys = newspaperMap.keySet();
        List<Article> allArticles = new ArrayList<>();
        for (String key : keys) {
            allArticles.addAll(parseAndInsertInDatabase(key));
        }

        convertAndSaveToJson(allArticles);
    }

    public List<Article> parseNewspaper(String newspaperName) {

        Logger logger = Logger.getInstance(ParseManager.class);

        List<Article> articles = new ArrayList<Article>();
        Parser newspaperParser = newspaperMap.get(newspaperName);
        newspaperParser.parse(articles);

        logger.logInfo("Ordenando la lista");
        Collections.sort(articles, comparator);
        logger.logInfo("Insertando anuncios");
        insertAds(newspaperParser.getName(), articles);

        return articles;
    }

    public List<Article> parseAndInsertInDatabase(String newspaperName) {
        DatabaseHelper dbHelper = DatabaseHelper.getInstance();
        List<Article> articles = parseNewspaper(newspaperName);

        if (articles != null && articles.size() > 0) {
            dbHelper.insertItemsInPath(newspaperName, articles);
            //writeToFile(articles);
        }

        return articles;
    }

    private void writeToFile(List<Article> articles) {
        for (Article article : articles) {
            writeToFile(article);
        }
    }

    private void convertAndSaveToJson(List<Article> articles) {
        String json = converListToJson(articles);
        writeJson(json);
    }

    private String converListToJson(List<Article> articles) {
        return new Gson().toJson(articles);
    }

    private void writeJson(String json) {
        BufferedWriter bw = null;
        FileWriter fw = null;

        try {
            fw = new FileWriter(new Date().getTime() + "_" + jsonFileName);
            bw = new BufferedWriter(fw);
            bw.write(json);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bw != null) {
                    bw.close();
                }

                if (fw != null) {
                    fw.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void writeToFile(Article article) {

        if (article.getType() != Types.ArticleTypes.AD.getValue()) {
            String filename = article.getPeriodico().replaceAll(" ", "_") + "_"
                              + article.getPubDate() + "_"
                              + article.getTitle().replaceAll(" ", "_");

            BufferedWriter bw = null;
            FileWriter fw = null;

            try {
                String content = article.getTitle() + "\n\n" + article.getDescription();
                fw = new FileWriter(filename);
                bw = new BufferedWriter(fw);
                bw.write(content);
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    if (bw != null)
                        bw.close();
                    if (fw != null)
                        fw.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void insertAds(String name, List<Article> list) {
        if (list.size() > 2) {
          //  list.add(1, new Article("", "", "", "", "", "", name, Types.ArticleTypes.AD.getValue(), ""));
         //   list.add(list.size() / 2, new Article("", "", "", "", "", "", name, Types.ArticleTypes.AD.getValue(), ""));
        }
    }

}