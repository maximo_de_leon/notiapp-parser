package parsers;

import com.rometools.rome.feed.synd.SyndContent;
import com.rometools.rome.feed.synd.SyndEntry;
import com.rometools.rome.feed.synd.SyndFeed;
import model.Article;
import model.Types;
import org.jsoup.Jsoup;
import util.Logger;

import java.io.IOException;
import java.util.List;

/**
 * Created by maximodeleon on 12/7/16.
 */
public class NoticiasSINParser extends ParserObject implements Parser {

    private final String NAME = "SIN";
    private final static String URL = "http://feeds.feedburner.com/noticiassin1";

    @Override
    public String getName() {
        return NAME;
    }

    public NoticiasSINParser() {
        super(URL);
    }

    @Override
    public void parse(List<Article> articles) {
        /*Logger logger = Logger.getInstance(NoticiasSINParser.class);
        SyndFeed feed = getFeed();
        if (feed == null) {
            logger.logWarning("Problema parseando el feed. No se van a procesar los articulos");
        } else {
            String newspaperImageUrl = "http://noticiassin.com/wp-content/themes/sin2016/imagenes/logo.png?a73acf";
            List<SyndEntry> entries = feed.getEntries();

            logger.logInfo(" Se van a empezar a parsear los articulos");

            int length = entries.size();
            int i = 1;
            int type = Types.ArticleTypes.IMAGE.getValue();

            for (SyndEntry entry : entries) {
                try {

                    String title = entry.getTitle();
                    SyndContent entryContent = entry.getDescription();
                    String description = completeHTML(entryContent.getValue());
                    String link = entry.getLink();
                    String pubDate = String.valueOf(entry.getPublishedDate().getTime());
                    String imagePath = Jsoup.connect(link).userAgent("Mozilla")
                            .get()
                            .select(".container")
                            .select(".row")
                            .select(".noticia-box")
                            .select(".col-md-8")
                            .select(".g-content")
                            .select(".nota-content")
                            .select(".nota-imagen")
                            .select("a")
                            .select("img")
                            .attr("src");

                    if (imagePath == null || (imagePath != null && imagePath.isEmpty())) {
                        type = Types.ArticleTypes.NOIMAGE.getValue();
                    }

                    articles.add(
                            new Article(title, description, imagePath, newspaperImageUrl
                                    , link, pubDate, NAME, type, Jsoup.parse(entryContent.getValue()).text())
                    );

                    logger.logInfo(" Procesado " + i + " de " + length);
                    i++;
                } catch (IOException e) {
                    logger.logError("Se ha producido un error", e);
                } catch (Exception e) {
                    logger.logError("Se ha producido un error", e);
                }
            }
            logger.logInfo(" Se parsearon todos los articulos");
        }*/
    }
}
