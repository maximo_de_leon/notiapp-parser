package parsers;

import com.rometools.rome.feed.synd.SyndEntry;
import com.rometools.rome.feed.synd.SyndFeed;
import model.Article;
import model.Types;
import org.jsoup.Jsoup;
import org.jsoup.select.Elements;
import util.Logger;

import java.io.IOException;
import java.util.List;

/**
 * Created by maximodeleon on 8/24/16.
 */
public class DiarioLibreParser extends ParserObject implements Parser {

    private final String NAME = "Diario Libre";
    private final static String URL = "https://www.diariolibre.com/rss/portada.xml";

    public DiarioLibreParser() {
        super(URL);
    }


    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void parse(List<Article> articles) {
        /*Logger logger = Logger.getInstance(DiarioLibreParser.class);
        SyndFeed feed = getFeed();
        if (feed == null) {
            logger.logWarning("Problema parseando el feed. No se van a procesar los articulos");
        } else {
            List<SyndEntry> entries = feed.getEntries();
            String newspaperImageUrl = feed.getImage().getUrl();
            logger.logInfo(" Se van a empezar a parsear los articulos");

            int length = entries.size();
            int i = 1;
            int type = Types.ArticleTypes.IMAGE.getValue();//

            for (SyndEntry entry : entries) {
                try {
                    String title = entry.getTitle();
                    String link = entry.getLink();
                    String pubDate = String.valueOf(entry.getPublishedDate().getTime());
                    Elements entryContent = Jsoup.connect(link).userAgent("Mozilla")
                            .get().select(".detalle-texto");
                    String description = completeHTML(entryContent.text());
                    String imagePath = "";

                    if (!entry.getEnclosures().isEmpty()) {
                        imagePath = entry.getEnclosures().get(0).getUrl();
                    } else {
                        type = Types.ArticleTypes.NOIMAGE.getValue();
                    }

                    articles.add(
                            new Article(title, description, imagePath, newspaperImageUrl, link, pubDate, NAME, type, entryContent.text())
                    );

                    logger.logInfo(" Procesado " + i + " de " + length);
                    i++;

                } catch (IOException e) {
                    logger.logError("Se ha producido un error", e);
                } catch (Exception e) {
                    logger.logError("Se ha producido un error", e);
                }
            }
            logger.logInfo(" Se parsearon todos los articulos");
        }*/
    }
}
