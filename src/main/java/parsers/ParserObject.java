package parsers;

import com.rometools.rome.feed.synd.SyndFeed;
import com.rometools.rome.io.FeedException;
import com.rometools.rome.io.SyndFeedInput;
import com.rometools.rome.io.XmlReader;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by maximodeleon on 8/24/16.
 */
abstract class ParserObject {

    private SyndFeed feed = null;
    private SyndFeedInput input;
    private URL feedUrlObject;

    private String HTMLopen = "<html><head><style>body{ font-size:100% !important; text-align: center !important;}</style></head>";
    private String HTMLBodyOpen = "<body>";
    private String HTMLclose = "</body></html>";

    public ParserObject(String feedUrl){
        try {

            feedUrlObject = new URL(feedUrl);
            input = new SyndFeedInput();
        }
        catch (MalformedURLException e) {
            System.out.print(
                    ParserObject.class.getCanonicalName()
                            + ": Error de Url:"
            );
            e.printStackTrace();

        }

    }

    public SyndFeed getFeed() {
        if (feed == null) {
            try {
                feed = input.build(new XmlReader(feedUrlObject));
            }
            catch (FeedException e){
                System.out.print(
                        ParserObject.class.getCanonicalName()
                                + ": Error con el feed:"
                );
                e.printStackTrace();
            }
            catch (IOException e){
                System.out.print(
                        ParserObject.class.getCanonicalName()
                                + ": Error de IO:"
                );
                e.printStackTrace();
            }
        }

        return feed;
    }

    public String completeHTML(String htmlPortion) {
        return htmlPortion;
    }

}
