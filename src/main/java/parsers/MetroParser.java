package parsers;

import com.rometools.rome.feed.synd.SyndEntry;
import com.rometools.rome.feed.synd.SyndFeed;
import main.Main;
import model.Article;
import model.Types;
import org.jsoup.Jsoup;
import util.Logger;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by maximodeleon on 10/21/16.
 */
public class MetroParser extends ParserObject implements Parser {


    private final String NAME = "Metro";
    private final static String URL = "https://www.metrord.do/do/rss/noticias.xml";
    private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public MetroParser() {super(URL);}

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void parse(List<Article> articles) {

        /*Logger logger = Logger.getInstance(MetroParser.class);
        SyndFeed feed = getFeed();
        if (feed == null) {
            logger.logWarning("Problema parseando el feed. No se van a procesar los articulos");
        } else {
            List<SyndEntry> entries = feed.getEntries();

            logger.logInfo(" Se van a empezar a parsear los articulos");
            int length = entries.size();
            int i = 1;


            int type = Types.ArticleTypes.IMAGE.getValue();
            String newspaperImageUrl = "http://www.metrord.do/locale/config/templates/img/global/logo.png";

            for (SyndEntry entry : entries) {

                try {
                    String title = entry.getTitle();
                    String link = entry.getLink();
                    String datetime = Jsoup.connect(link).userAgent("Mozilla")
                            .get().select(".main-content")
                            .select(".wrapper")
                            .select("time")
                            .get(0).
                            attr("datetime");
                    datetime = datetime.replace("T", " ");
                    datetime = datetime.substring(0, datetime.length() - 6);
                    String pubDate = String.valueOf(sdf.parse(datetime).getTime());
                    String description =
                            Jsoup.connect(link).userAgent("Mozilla")
                                    .get().select(".main-content")
                                    .select(".wrapper").select(".body-content").text();;
                    String imagePath = "";


                    if (!entry.getEnclosures().isEmpty()) {
                        imagePath = entry.getEnclosures().get(0).getUrl();
                    } else {
                        type = Types.ArticleTypes.NOIMAGE.getValue();
                    }

                    articles.add(
                            new Article(title, description, imagePath, newspaperImageUrl, link, pubDate, NAME, type, description)
                    );

                    logger.logInfo(" Procesado " + i + " de " + length);
                    i++;
                } catch (Exception e) {
                    logger.logError("Se ha producido un error", e);
                }
            }
        }*/
    }
}
