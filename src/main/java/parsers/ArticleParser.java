package parsers;

import com.rometools.rome.feed.synd.SyndEntry;
import de.l3s.boilerpipe.extractors.ArticleExtractor;
import model.Article;
import model.Types;
import org.apache.commons.lang.StringUtils;
import org.apache.tika.io.TikaInputStream;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.parser.html.BoilerpipeContentHandler;
import org.apache.tika.sax.BodyContentHandler;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.DateTimeFormatterBuilder;
import org.joda.time.format.DateTimeParser;
import org.jsoup.Jsoup;
import textsummary.SummaryTool;

import java.io.InputStream;
import java.net.URL;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class ArticleParser {

    DateTimeParser [] parsers = {
            DateTimeFormat.forPattern("YYYY-MM-DD HH:mm:ssZ") .getParser(),
            DateTimeFormat.forPattern("YYYY-MM-DD HH:mm:ss") .getParser(),
            DateTimeFormat.forPattern("EEE, d MMM yyyy HH:mm:ss z") .getParser()
    };

    String [] wantedStrings = {
            "Publicidad"
            ,"Por:"
            ,"Se el primero en comentar"
            ,"Fuente: "
            ,"Deja un comentario"
            ,"Cancelar respuesta"
            ,"Tu dirección de correo electrónico no será publicada"
            ,"You may use these HTML tags and attributes"
            ,"Nombre *"
            ,"Web"
            ,"Comentario"
            ,"Más de Tu Salud"
            ,"Artículo Siguiente"
            ,"Comparte"
            ,"Contenido relacionado"
            ,"COMENTARIOS"
            ,"Usuario"
            ,"Más leídas"
            ,"Please enable JavaScript to view the comments powered by Disqus."
    };

    /**
     *
     * Busca en el objeto metadata la información que le envia por parámetro
     *
     * @param metadata Objecto metadata para buscar
     * @param field campo que se desea retornar
     * @param failoverFiled en caso de que el campo field retorne nulo, se usa este campo
     * @return El valor encontrado en el meta.
     */
    private String getMeta(Metadata metadata, String field, String failoverFiled) {

        String meta = metadata.get(field);

        if (meta == null || meta.isEmpty()) {
            if (failoverFiled != null && !failoverFiled.isEmpty() ) {
                meta = metadata.get(failoverFiled);
            }
        } else if (meta != null && StringUtils.isAlpha(meta)){
            meta = metadata.get(failoverFiled);
        }
        return meta;
    }

    /**
     *
     * Saca la fecha de publicación de cada articulo. Si no se encuentra el información en los meta
     * de la página que se parsea, lo busca en el feed y lo devuelve, de lo contrario, lo intenta
     * parsear con joda time y devuelve el valor parseaod. Si no funcionan la opciones anteriores,
     *  se retorna null
     *
     * @param entry Objecto SyndEntry que tiene la fecha de publicación
     * @param metadata Objecto metadata para buscar en él la fecha de publicación
     * @param field Nombre del meta que tiene la fecha
     * @param failoverField En caso de que el campo field no se encuentre, se use este campo
     * @return Retorna la fecha en formato unix
     */
    private Long getDate(SyndEntry entry, Metadata metadata, String field, String failoverField) {

        String dateString = getMeta(metadata, field, failoverField);

        DateTimeFormatter formatter = new DateTimeFormatterBuilder()
                .append(null, parsers).toFormatter();

        if (dateString == null) {

            Date dateObject = entry.getPublishedDate();

            if (dateObject == null) {
                dateString = getMeta(metadata,"article:published_time", "article:updated_time");
                if (dateString != null) {
                  DateTime date = formatter.parseDateTime(dateString.replace("T", " "));
                  return date.getMillis();
                }
            } else {
                return dateObject.getTime();
            }
        } else {

            try {
                DateTime date = formatter.parseDateTime(dateString.replace("T", " "));
                return date.getMillis();
            } catch (Exception e) {

            }
        }

        return null;
    }


    /**
     * Retorna si una cadena de caracteres empieza con caracteres no deseadpos.
     *
     * @param sentece String a evaluar
     * @return True si la cadena empieza con algun caracter no requerido, False en caso contrario
     */
    private boolean startsWithAnyNotWantedString(String sentece) {
        for (String unWanted : wantedStrings) {
            if (sentece.startsWith(unWanted)) {
                return true;
            }
        }
       return false;
    }
    /**
     * Quita del texto palabras que se consideran no necesarios.
     *
     * @param content Contenido a limpiar
     * @return Contenido limpio
     */
    private String cleanContent(String content) {
        String [] paragraphs = StringUtils.split(content, "\n");
        StringBuffer buffer = new StringBuffer();
        for (String sentence : paragraphs) {

            if (sentence.length() == 1){
                continue;
            }
            else if (startsWithAnyNotWantedString(sentence)){
                continue;
            }

            buffer.append(sentence + "\n");
        }

        return buffer.toString();
    }

    /**
     *
     * @param content
     * @return
     */
    private int getFirstWordsIndex(String content) {
        if (content != null && !content.isEmpty()) {
            Pattern pattern = Pattern.compile("\\.\\s*–|-");
            Matcher matcher = pattern.matcher(content);

            if (matcher.find()){
                return matcher.start();
            }
        }

        return 0;
    }

    /**
     *
     * Trata de tomar solo la parte del articulo que interesa
     * al programa, utilizando la descripcion que está en el feed
     *
     * @param content El handler con el texto extraido
     * @param entry El syndfeed para encontrar la descripción del rss
     * @return Texto con los caracteres de más removidos, en caso que se hayan podido remover, sino
     * retorna el texo original que se pasó por parámetro
     */
    private String parseContent(BodyContentHandler content, SyndEntry entry, Metadata metadata){

        String bodyContentConent = cleanContent(content.toString());
        String syndFeedContent = entry.getDescription().getValue();
        String description = getMeta(metadata,"description", "og:description");

        //Primero comparo con metada
        if (description != null && !description.isEmpty()) {
            int index = bodyContentConent.indexOf(description.substring(0, 50));

            if (index >= 0) {
                return bodyContentConent.substring(index);
            }
        }

        // Luego comparo con lo que hay en el feed, si no encontre con metadata
        if (syndFeedContent != null && syndFeedContent != "") {
            syndFeedContent = Jsoup.parse(syndFeedContent).text();
            int index = getFirstWordsIndex(bodyContentConent);

            if (index >= 0)  {
                return bodyContentConent.substring(index);
            } else {
                index = bodyContentConent.indexOf(syndFeedContent.substring(0, 30));
                if (index  >= 0) {
                    return bodyContentConent.substring(index);
                }
            }
        }

        return bodyContentConent;
    }

    /**
     *
     *  Parseador de noticias. Saca el titulo, descripcion, imagen
     *  url y fecha de publicacion de cada link que se le pasa
     *  y crea un object articulo que retorna a quien lo llama
     *
     *
     * @param url url para parsear
     * @param entry objecto SyndEntry utilizado para buscar algunas informaciones
     * @param newspaperName Nombre del periodico que se está parseando
     * @return Objecto Article con la información parseada.
     */
    public Article parse(String url, SyndEntry entry, String newspaperName) {


        AutoDetectParser parser = new AutoDetectParser();
        Metadata metadata = new Metadata();
        BodyContentHandler handler = new BodyContentHandler();
        InputStream stream = null;
        try {
            stream = TikaInputStream.get(new URL(url));
            parser.parse(stream, new BoilerpipeContentHandler(handler, new ArticleExtractor()), metadata);

            String title = getMeta(metadata, "title", "og:title");
            String imagen = getMeta(metadata,"og:image", null);
            String descripcion = parseContent(handler, entry, metadata);
            SummaryTool textSummary = new SummaryTool();
            textSummary.createSummary(descripcion);

            String shortDesc = textSummary.getSummary();

            Long pubDate = getDate(entry, metadata,"published_date", "modified_date");
            int type = Types.ArticleTypes.NOIMAGE.getValue();
            return  new Article(title, descripcion, imagen, url, pubDate, newspaperName, type, shortDesc);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                stream.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return null;
    }

}
