package parsers;

import com.rometools.rome.feed.synd.SyndEntry;
import com.rometools.rome.feed.synd.SyndFeed;
import com.rometools.rome.io.FeedException;
import com.rometools.rome.io.SyndFeedInput;
import com.rometools.rome.io.XmlReader;
import main.Main;
import model.Article;
import util.Logger;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class FeedParser {

    /**
     *
     * Para obtener la url redireccionada en caso de que haya redireccionamiento.
     *
     * @param url url orignal desde donde es posible que se redireccione. Aqui va lo que está en el feed.
     * @return la url final que no redirecciona a ningún lado.
     */
    private String getFinalUrl(String url) {
        try {
            HttpURLConnection con = (HttpURLConnection) new URL(url).openConnection();
            con.setInstanceFollowRedirects(false);
            con.connect();;
            con.getInputStream();

            if (con.getResponseCode() == HttpURLConnection.HTTP_MOVED_PERM
                    || con.getResponseCode() == HttpURLConnection.HTTP_MOVED_TEMP) {
                String redirectedUrl = con.getHeaderField("Location");
                return redirectedUrl;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return url;
    }

    /**
     *
     * Parsea el feed con todos los articulos que contenga.
     *
     * @param feedUrl Url del feed rss.
     * @param newspaperName Nombre del periodico del feed.
     * @return Lista de articulos extraidos del feed.
     */
    public List<Article> parse(String feedUrl, String newspaperName) {

        Logger logger = Logger.getInstance(Main.class);
        List<Article> articles = new ArrayList<>();
        SyndFeed feed;
        SyndFeedInput input;
        URL feedUrlObject;

        try {
            feedUrlObject = new URL(feedUrl);
            input = new SyndFeedInput();
            feed = input.build(new XmlReader(feedUrlObject));

            if (feed == null) {
                logger.logWarning("Problema parseando el feed. No se van a procesar los articulos");
            } else {
                List<SyndEntry> entries = feed.getEntries();
                for (SyndEntry entry : entries) {
                    logger.logInfo("Parseado articulo - " + entry.getLink());
                    ArticleParser parser = new ArticleParser();

                    String url = getFinalUrl(entry.getLink());
                    Article article = parser.parse(url , entry, newspaperName);
                    articles.add(article);
                }
            }

        }  catch (MalformedURLException e) {
            logger.logError(e.getMessage());
        } catch (FeedException e){
            logger.logError(e.getMessage());
        }
        catch (IOException e){
            logger.logError(e.getMessage());
        }

        return articles;
    }
}
