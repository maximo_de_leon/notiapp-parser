package parsers;

import model.Article;

import java.util.List;

/**
 * Created by maximodeleon on 8/23/16.
 */
public interface Parser {
    public void parse(List<Article> articles);
    public String getName();
}
