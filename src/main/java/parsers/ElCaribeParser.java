package parsers;

import com.rometools.rome.feed.synd.SyndEntry;
import com.rometools.rome.feed.synd.SyndFeed;
import model.Article;
import model.Types;
import org.jsoup.Jsoup;
import org.jsoup.select.Elements;
import util.Logger;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

/**
 * Created by maximodeleon on 8/24/16.
 */
public class ElCaribeParser extends ParserObject implements Parser {

    private final String NAME = "El Caribe";
    private final static String URL = "http://www.elcaribe.com.do/rss";

    public ElCaribeParser(){
        super(URL);
    }
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void parse(List<Article> articles) {

       /* Logger logger = Logger.getInstance(ElCaribeParser.class);
        SyndFeed feed = getFeed();
        if (feed == null) {
            logger.logWarning("Problema parseando el feed. No se van a procesar los articulos");
        } else {
            logger.logInfo(" Se van a empezar a parsear los articulos");
            List<SyndEntry> entries = feed.getEntries();
            String newspaperImageUrl = "https://storage.googleapis.com/mmc-elcaribe-bucket/uploads/2017/07/273b12e9-elcaribe-logo-3x.png";//feed.getImage().getUrl();
            int length = entries.size();
            int i = 1;


            int type = Types.ArticleTypes.IMAGE.getValue();

            for (SyndEntry entry : entries) {
                try {
                    String title = entry.getTitle();
                    String link = entry.getLink().replace("\t", "").replace("\n", "");
                    String pubDate = String.valueOf(entry.getPublishedDate().getTime());


                    Elements entryContent = Jsoup.connect(link).userAgent("Mozilla")
                            .get().select(".cuerpoNoticia");
                    String description = completeHTML(entry.getDescription().getValue());
                    String imagePath = "";

                    if (!entry.getEnclosures().isEmpty()) {
                        imagePath = entry.getEnclosures().get(0).getUrl();
                        if (imagePath == null || imagePath.isEmpty()) {
                            imagePath = Jsoup.connect(link).userAgent("Mozilla")
                                    .get().select(".contenidoArticulo")
                                    .select(".columnaContenido")
                                    .select(".contenidoArticulo")
                                    .select(".multimediaCompartir")
                                    .select("img").attr("src");
                        }
                    } else {
                        type = Types.ArticleTypes.NOIMAGE.getValue();
                    }

                    articles.add(
                            new Article(title, description, imagePath, newspaperImageUrl, link, pubDate, NAME, type, entryContent.text())
                    );

                    logger.logInfo(" Procesado " + i + " de " + length);
                    i++;
                } catch (IOException e) {
                    logger.logError("Se ha producido un error", e);
                } catch (Exception e) {
                    logger.logError("Se ha producido un error", e);
                }
            }
        }*/
    }
}
