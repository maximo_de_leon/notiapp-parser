package parsers;

import com.rometools.rome.feed.synd.SyndEntry;
import com.rometools.rome.feed.synd.SyndFeed;
import model.Article;
import model.Types;
import org.jsoup.Jsoup;
import org.jsoup.select.Elements;
import util.Logger;

import java.util.List;

/**
 * Created by maximodeleon on 11/25/16.
 */
public class ElNacionalParser extends ParserObject implements Parser  {

    private final String NAME = "El Nacional";
    private final static String URL = "http://elnacional.com.do/feed/";

    public ElNacionalParser() { super(URL);}

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void parse(List<Article> articles) {
        /*Logger logger = Logger.getInstance(ElNacionalParser.class);
        SyndFeed feed = getFeed();
        if (feed == null) {
            logger.logWarning("Problema parseando el feed. No se van a procesar los articulos");
        } else {
            List<SyndEntry> entries = feed.getEntries();
            String newspaperImageUrl = feed.getImage().getUrl();
            logger.logInfo(" Se van a empezar a parsear los articulos");

            int length = entries.size();
            int i = 1;
            int type = Types.ArticleTypes.IMAGE.getValue();//

            for (SyndEntry entry : entries) {
                try {
                    String title = entry.getTitle();
                    String link = entry.getLink();
                    String pubDate = String.valueOf(entry.getPublishedDate().getTime());
                    Elements conn = Jsoup.connect(link).userAgent("Mozilla")
                            .get().select(".main-content")
                            .select(".container")
                            .select(".row")
                            .select(".main-section");
                    Elements entryContent = conn
                            .select(".featured-post")
                            .select(".row")
                            .select(".post-text");
                    String description = completeHTML(entryContent.text());

                    String imagePath = "";

                    if (!entry.getEnclosures().isEmpty()) {
                        imagePath = entry.getEnclosures().get(0).getUrl();
                    } else {

                        imagePath =  conn
                                .select(".featured-post-picture")
                                .select("img").attr("src");

                        if (imagePath == null || imagePath.isEmpty()) {
                            type = Types.ArticleTypes.NOIMAGE.getValue();
                        }
                    }

                    articles.add(
                            new Article(title, description, imagePath, newspaperImageUrl, link, pubDate, NAME, type, entryContent.text())
                    );

                    logger.logInfo(" Procesado " + i + " de " + length);
                    i++;
                }catch (Exception e) {
                    logger.logError("Se ha producido un error", e);
                }
            }
            logger.logInfo(" Se parsearon todos los articulos");
         }*/
    }
}
