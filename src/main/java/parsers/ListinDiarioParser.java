package parsers;

import com.rometools.rome.feed.synd.SyndEntry;
import com.rometools.rome.feed.synd.SyndFeed;
import model.Article;
import model.Types;
import org.jsoup.Jsoup;
import org.jsoup.select.Elements;
import util.Logger;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

/**
 * Created by maximodeleon on 8/24/16.
 */
public class ListinDiarioParser extends ParserObject implements Parser {


    private final String NAME = "Listin Diario";
    private final static String URL = "http://www.listindiario.com/rss/portada/";

    public ListinDiarioParser() {
        super(URL);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void parse(List<Article> articles) {
/*
        Logger logger = Logger.getInstance(ListinDiarioParser.class);
        SyndFeed feed = getFeed();
        if (feed == null) {
            logger.logWarning("Problema parseando el feed. No se van a procesar los articulos");
        } else {

            List<SyndEntry> entries = feed.getEntries();
            String newspaperImageUrl = "http://www.listindiario.com/Themes/Default/Content/img/opiniondefault.png";


            logger.logInfo(" Se van a empezar a parsear los articulos");
            int length = entries.size();
            int i = 1;
            int type = Types.ArticleTypes.IMAGE.getValue();


            SimpleDateFormat df = new SimpleDateFormat("dd/mm/yyyy hh:mm:ss a");

            String pubDate = Calendar.getInstance().get(Calendar.DAY_OF_MONTH)
                    + "/"
                    + Calendar.getInstance().get(Calendar.MONTH)
                    + "/"
                    + Calendar.getInstance().get(Calendar.YEAR)
                    + " 12:00:00 am";

            try {
                pubDate = String.valueOf(df.parse(pubDate).getTime());
            } catch (Exception e) {
                logger.logError("Se ha producido un error", e);
            }

            for (SyndEntry entry : entries) {
                try {
                    String title = entry.getTitle();
                    String link = entry.getLink();

                    Elements entryContent = Jsoup.connect(link).userAgent("Mozilla")
                            .get()
                            .select("#ArticleBody");
                    String description = completeHTML(entryContent.text());
                    String imagePath = "";

                    if (!entry.getEnclosures().isEmpty()) {
                        imagePath = entry.getEnclosures().get(0).getUrl();
                    } else {
                        type = Types.ArticleTypes.NOIMAGE.getValue();
                    }


                    articles.add(
                            new Article(title, description, imagePath, newspaperImageUrl, link, pubDate, NAME, type, entryContent.text())
                    );

                    logger.logInfo(" Procesado " + i + " de " + length);
                    i++;
                } catch (IOException e) {
                    logger.logError("Se ha producido un error", e);
                } catch (Exception e) {
                    logger.logError("Se ha producido un error", e);
                }
            }

            logger.logInfo(" Se parsearon todos los articulos");
        }*/
    }
}
