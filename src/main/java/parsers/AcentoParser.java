package parsers;

import com.rometools.rome.feed.synd.SyndEntry;
import com.rometools.rome.feed.synd.SyndFeed;
import model.Article;
import model.Types;
import org.jsoup.Jsoup;
import org.jsoup.select.Elements;
import util.Logger;

import java.util.List;

/**
 * Created by maximodeleon on 11/24/16.
 */
public class AcentoParser extends ParserObject implements Parser  {

    private final String NAME = "Acento";
    private final static String URL = "http://acento.com.do/feed/";

    public AcentoParser() {
        super(URL);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void parse(List<Article> articles) {
/*
        Logger logger = Logger.getInstance(AcentoParser.class);
        SyndFeed feed = getFeed();
        if (feed == null) {
            logger.logWarning("Problema parseando el feed. No se van a procesar los articulos");
        } else {
            List<SyndEntry> entries = feed.getEntries();
            String newspaperImageUrl = "http://acento-main-cdn.odsoluciones.netdna-cdn.com/wp-content/uploads/acento_avatar_1440501773-96x96.png";
            logger.logInfo(" Se van a empezar a parsear los articulos");

            int length = entries.size();
            int i = 1;
            int type = Types.ArticleTypes.IMAGE.getValue();//

            for (SyndEntry entry : entries) {
                try {


                    String title = entry.getTitle();
                    String link = entry.getLink();

                    Elements conn = Jsoup.connect(link).userAgent("Mozilla")
                            .get().select(".wrapper")
                            .select("main")
                            .select(".single-article")
                            .select(".container")
                            .select("#article-group")
                            .select(".article-container")
                            .select(".single-article-row")
                            .select(".article-content-container")
                            .select("article");

                    Elements entryContent = conn
                            .select(".the-content");

                    String pubDate = String.valueOf(entry.getPublishedDate().getTime());

                    String description = completeHTML(entryContent.toString());
                    String imagePath = "";

                    if (!entry.getEnclosures().isEmpty()) {
                        imagePath = entry.getEnclosures().get(0).getUrl();
                    } else {

                        imagePath =  conn
                                .select(".the-thumbnail")
                                .select("img").attr("src");

                        if (imagePath == null || imagePath.isEmpty()) {
                            type = Types.ArticleTypes.NOIMAGE.getValue();
                        }
                    }

                    articles.add(
                            new Article(title, description, imagePath, newspaperImageUrl, link, pubDate, NAME, type, entryContent.text())
                    );

                    logger.logInfo(" Procesado " + i + " de " + length);
                    i++;

                }catch (Exception e) {
                    logger.logError("Se ha producido un error", e);
                }
            }

            logger.logInfo(" Se parsearon todos los articulos");

        }*/
    }
}
